source 'https://rubygems.org'

gem 'dotenv-rails', :groups => [:development, :test]
# gem 'capistrano-dotenv-tasks'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'devise'
gem 'activeadmin', github: 'activeadmin'
gem 'active_admin_importable'
# gem "meta_search", '>= 1.1.0.pre'

gem 'timeliness'

# For editable email template
gem 'liquid'
gem "letter_opener", :group => :development
gem 'mailgun_rails'

gem 'httparty'

gem 'ckeditor'
gem 'paperclip'
gem 'aws-sdk-v1'

gem "haml"
gem "haml-rails", "~> 0.9"
gem 'validates_overlap'
gem 'mysql2'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'rspec-rails', '~> 3.0'
  gem 'factory_girl'
  gem 'factory_girl_rails'
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
end

group :test do
  gem 'database_cleaner'
  gem 'json_matchers'
  gem 'shoulda-matchers', '~> 3.1'

  gem 'capybara'
  gem 'poltergeist'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem "bullet"
end

group :production do
  gem 'pg', '0.15.1'
  gem 'rails_12factor'
end

group :development do
    gem 'capistrano',         require: false
    gem 'capistrano-rvm',     require: false
    gem 'capistrano-rails',   require: false
    gem 'capistrano-bundler', require: false
    gem 'capistrano3-puma',   require: false
end

gem 'puma'

gem 'paper_trail'

