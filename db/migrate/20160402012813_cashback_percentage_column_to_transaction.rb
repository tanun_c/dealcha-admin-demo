class CashbackPercentageColumnToTransaction < ActiveRecord::Migration
  def change

    add_column :stores, :cashback_type, :string, :default => :fixed
    
    add_column :transactions, :cashback_type, :string, :default => :fixed
    add_column :transactions, :cashback_amount, :decimal

  end
end
