class CreateEmailTemplates < ActiveRecord::Migration
  def change
    create_table :email_templates do |t|
      t.string :key
      t.text :description
      t.string :locale, limit: 3
      t.text :body
      t.timestamps null: false
    end
  end
end