class AddPaperclipToStore < ActiveRecord::Migration
  def change
  	add_attachment :stores, :thumbnail_image
  end
end
