class PromoCode < ActiveRecord::Migration
  def change

  	create_table :promotion_code_campaigns do |t|
      t.string :name
      t.datetime :expiration
      t.string :award_type, default: 'amount' # amount or percent
      t.decimal :award_amount, default: 0.0
      t.decimal :award_percent, default: 0.0
      t.timestamps null: false
    end
  	
  	create_table :promotion_codes do |t|
      t.string :code, null: false
      t.integer :limit, default: 0
      t.integer :promotion_code_campaign_id
      t.timestamps null: false
    end

    create_table :promotion_code_usages do |t|
      t.datetime :consumed
      t.integer :user_id
      t.integer :promotion_code_id
    end
    
  end
end
