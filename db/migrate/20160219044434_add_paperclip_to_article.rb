class AddPaperclipToArticle < ActiveRecord::Migration
  def change
  	add_attachment :articles, :thumbnail_image
  end
end
