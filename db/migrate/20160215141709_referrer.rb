class Referrer < ActiveRecord::Migration
  def change

    create_table :referral_campaigns do |t|
      t.string :name
      t.datetime :campaign_start
      t.datetime :campaign_end
      t.decimal :award_amount, default: 0.0 # amount of award in THB
      t.decimal :grace_period, default: 3.0 # month that allow the newly registered to get his first cashback that referral is awarded
      t.string :condition, default: 'cashback' # 'cashback' or 'registered'
    end

    create_table :referrals do |t|
      t.integer :referrer_id # id of the referrer
      t.integer :referree_id # id of the referree
      t.integer :campaign_id # id of the referral campaign
      t.boolean :claimed  # claimed or not
    end

  end
end