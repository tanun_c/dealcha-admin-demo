class StoreUrlHandling < ActiveRecord::Migration
  def change
  	add_column :stores, :provider, :string, :limit => 32 # accesstrade, lazada, cj, ...
  	add_column :stores, :deal_url, :string
  	add_column :stores, :main_url, :string
  end
end
