class AddIsLazadaNewUserToCashback < ActiveRecord::Migration
  def change
    add_column :user_cashback, :is_lazada_new_user, :boolean
  end
end
