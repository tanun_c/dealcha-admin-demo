class CreateConversions < ActiveRecord::Migration
  def change
    create_table :conversions do |t|
      t.integer :trip_id
      t.integer :diff_id
      t.decimal :purchase
      t.decimal :amount
      t.string :status
      t.text :info
      t.timestamps null: false
    end

    add_column :user_cashback, :conversion_id, :integer
  end
end