# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160508024039) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "articles", force: :cascade do |t|
    t.integer  "creator_id",                   limit: 4,     default: 1,             null: false
    t.string   "title",                        limit: 255,   default: "new article", null: false
    t.text     "content",                      limit: 65535
    t.boolean  "published",                                  default: false,         null: false
    t.string   "content_type",                 limit: 255,   default: "article",     null: false
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.string   "key",                          limit: 255
    t.string   "thumbnail",                    limit: 255
    t.string   "thumbnail_image_file_name",    limit: 255
    t.string   "thumbnail_image_content_type", limit: 255
    t.integer  "thumbnail_image_file_size",    limit: 4
    t.datetime "thumbnail_image_updated_at"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",        limit: 255,   null: false
    t.text     "description", limit: 65535
    t.text     "preference",  limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "commentable_id",   limit: 4,     null: false
    t.string   "commentable_type", limit: 255,   null: false
    t.integer  "user_id",          limit: 4,     null: false
    t.text     "comment",          limit: 65535, null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "contests", force: :cascade do |t|
    t.string   "name",       limit: 255,                null: false
    t.datetime "start",                                 null: false
    t.datetime "end",                                   null: false
    t.boolean  "active",                 default: true, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "conversions", force: :cascade do |t|
    t.integer  "trip_id",    limit: 4
    t.integer  "diff_id",    limit: 4
    t.decimal  "purchase",                 precision: 10
    t.decimal  "amount",                   precision: 10
    t.string   "status",     limit: 255
    t.text     "info",       limit: 65535
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "coupons", force: :cascade do |t|
    t.string   "name",        limit: 255,   null: false
    t.string   "url",         limit: 255
    t.string   "code",        limit: 255
    t.text     "description", limit: 65535
    t.string   "store_name",  limit: 255
    t.integer  "store_id",    limit: 4
    t.integer  "creator_id",  limit: 4
    t.text     "instruction", limit: 65535
    t.datetime "expire_on"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "deals", force: :cascade do |t|
    t.string   "name",                 limit: 255,                           default: "",        null: false
    t.string   "url",                  limit: 1000
    t.text     "deal_description",     limit: 65535
    t.text     "product_description",  limit: 65535
    t.text     "cover_image",          limit: 65535
    t.text     "original_description", limit: 65535
    t.integer  "creator_id",           limit: 4,                                                 null: false
    t.integer  "curator_id",           limit: 4
    t.integer  "shop_id",              limit: 4
    t.string   "shop_name",            limit: 255
    t.string   "shop_location",        limit: 255
    t.integer  "category_id",          limit: 4,                                                 null: false
    t.boolean  "is_featured"
    t.datetime "is_featured_on"
    t.boolean  "is_today_deal"
    t.datetime "is_today_deal_on"
    t.string   "deal_type",            limit: 255
    t.decimal  "price",                              precision: 8, scale: 2
    t.decimal  "original_price",                     precision: 8, scale: 2
    t.decimal  "discount_percent",                   precision: 8, scale: 2
    t.integer  "bg_buy",               limit: 4
    t.integer  "bg_get",               limit: 4
    t.string   "coupon_code",          limit: 255
    t.integer  "vote_count",           limit: 4
    t.integer  "comment_count",        limit: 4
    t.datetime "count_updated"
    t.datetime "expire_on"
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.boolean  "active",                                                     default: true,      null: false
    t.string   "where",                limit: 255,                           default: "offline", null: false
    t.string   "location",             limit: 255
  end

  create_table "email_templates", force: :cascade do |t|
    t.string   "key",         limit: 255
    t.text     "description", limit: 65535
    t.string   "locale",      limit: 3
    t.text     "body",        limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "favoriteable_id",   limit: 4,   null: false
    t.string   "favoriteable_type", limit: 255, null: false
    t.integer  "user_id",           limit: 4,   null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "features", force: :cascade do |t|
    t.integer  "featureable_id",   limit: 4,   null: false
    t.string   "featureable_type", limit: 255, null: false
    t.string   "feature",          limit: 255
    t.date     "for"
    t.integer  "position",         limit: 4,   null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "flags", force: :cascade do |t|
    t.integer  "flagable_id",   limit: 4,     null: false
    t.string   "flagable_type", limit: 255,   null: false
    t.integer  "user_id",       limit: 4,     null: false
    t.text     "reason",        limit: 65535
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "forum_threads", force: :cascade do |t|
    t.string   "title",      limit: 255,                  null: false
    t.text     "body",       limit: 65535,                null: false
    t.integer  "creator_id", limit: 4,                    null: false
    t.boolean  "active",                   default: true, null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "images", force: :cascade do |t|
    t.text     "description",    limit: 65535
    t.string   "location",       limit: 255,   null: false
    t.integer  "owner_id",       limit: 4,     null: false
    t.integer  "imageable_id",   limit: 4,     null: false
    t.string   "imageable_type", limit: 255,   null: false
    t.string   "role",           limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "info_fields", force: :cascade do |t|
    t.integer  "user_id",     limit: 4,     null: false
    t.string   "field_type",  limit: 255,   null: false
    t.text     "field_value", limit: 65535, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "migrations", id: false, force: :cascade do |t|
    t.string  "migration", limit: 255, null: false
    t.integer "batch",     limit: 4,   null: false
  end

  create_table "oauth", force: :cascade do |t|
    t.string   "site",       limit: 255, null: false
    t.string   "token",      limit: 255, null: false
    t.string   "oauth_id",   limit: 255, null: false
    t.integer  "user_id",    limit: 4,   null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "password_resets", id: false, force: :cascade do |t|
    t.string   "email",      limit: 255, null: false
    t.string   "token",      limit: 255, null: false
    t.datetime "created_at",             null: false
  end

  add_index "password_resets", ["email"], name: "password_resets_email_index", using: :btree
  add_index "password_resets", ["token"], name: "password_resets_token_index", using: :btree

  create_table "postbacks", force: :cascade do |t|
    t.datetime "received"
    t.datetime "imported"
    t.text     "information", limit: 65535, null: false
    t.integer  "store_id",    limit: 4,     null: false
    t.string   "source",      limit: 255,   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "promotion_code_campaigns", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.datetime "expiration"
    t.string   "award_type",    limit: 255,                default: "amount"
    t.decimal  "award_amount",              precision: 10, default: 0
    t.decimal  "award_percent",             precision: 10, default: 0
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
  end

  create_table "promotion_code_usages", force: :cascade do |t|
    t.datetime "consumed"
    t.integer  "user_id",           limit: 4
    t.integer  "promotion_code_id", limit: 4
  end

  create_table "promotion_codes", force: :cascade do |t|
    t.string   "code",                       limit: 255,             null: false
    t.integer  "limit",                      limit: 4,   default: 0
    t.integer  "promotion_code_campaign_id", limit: 4
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  create_table "redeems", force: :cascade do |t|
    t.integer  "redeemable_id",   limit: 4,   null: false
    t.string   "redeemable_type", limit: 255, null: false
    t.string   "session_id",      limit: 255
    t.integer  "user_id",         limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "referral_campaigns", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.datetime "campaign_start"
    t.datetime "campaign_end"
    t.decimal  "award_amount",               precision: 10, default: 0
    t.decimal  "grace_period",               precision: 10, default: 3
    t.string   "condition",      limit: 255,                default: "cashback"
  end

  create_table "referrals", force: :cascade do |t|
    t.integer "referrer_id", limit: 4
    t.integer "referree_id", limit: 4
    t.integer "campaign",    limit: 4
    t.boolean "claimed"
  end

  create_table "rep_points", force: :cascade do |t|
    t.decimal  "value",                precision: 8, scale: 2, default: 0.0, null: false
    t.integer  "user_id",    limit: 4,                                       null: false
    t.integer  "contest_id", limit: 4,                                       null: false
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
  end

  create_table "scraper_fields", force: :cascade do |t|
    t.integer  "store_id",   limit: 4,   null: false
    t.string   "field",      limit: 255
    t.string   "xpath",      limit: 255
    t.string   "attr",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "social_links", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,               null: false
    t.string   "type",       limit: 255,             null: false
    t.string   "url",        limit: 255,             null: false
    t.integer  "order",      limit: 4,   default: 0, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "stores", force: :cascade do |t|
    t.string   "name",                         limit: 255,                                             null: false
    t.text     "description",                  limit: 65535
    t.string   "flag",                         limit: 255
    t.string   "friendly_id",                  limit: 255
    t.boolean  "is_featured",                                                        default: false,   null: false
    t.decimal  "cashback",                                   precision: 8, scale: 2, default: 0.0,     null: false
    t.datetime "created_at",                                                                           null: false
    t.datetime "updated_at",                                                                           null: false
    t.string   "url",                          limit: 255
    t.string   "image",                        limit: 255
    t.string   "thumbnail_image_file_name",    limit: 255
    t.string   "thumbnail_image_content_type", limit: 255
    t.integer  "thumbnail_image_file_size",    limit: 4
    t.datetime "thumbnail_image_updated_at"
    t.string   "cashback_type",                limit: 255,                           default: "fixed"
    t.string   "provider",                     limit: 32
    t.string   "deal_url",                     limit: 255
    t.string   "main_url",                     limit: 255
  end

  create_table "taggables", id: false, force: :cascade do |t|
    t.integer  "tag_id",        limit: 4,   null: false
    t.integer  "taggable_id",   limit: 4,   null: false
    t.string   "taggable_type", limit: 255, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string   "tag_value",  limit: 255, null: false
    t.string   "tag_type",   limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.datetime "occured"
    t.text     "destination",     limit: 65535
    t.integer  "user_id",         limit: 4
    t.integer  "store_id",        limit: 4,                                      null: false
    t.boolean  "cashback",                                     default: false,   null: false
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.string   "cashback_type",   limit: 255,                  default: "fixed"
    t.decimal  "cashback_amount",               precision: 10
  end

  create_table "translation_content", force: :cascade do |t|
    t.integer  "content_id",   limit: 4,     null: false
    t.string   "content_type", limit: 255,   null: false
    t.string   "locale",       limit: 255,   null: false
    t.string   "field",        limit: 255,   null: false
    t.text     "translated",   limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "translation_interface", force: :cascade do |t|
    t.string   "key",         limit: 255,   null: false
    t.string   "context",     limit: 255,   null: false
    t.text     "translation", limit: 65535
    t.string   "description", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "user_cashback", force: :cascade do |t|
    t.integer  "user_id",            limit: 4,                            null: false
    t.datetime "registered",                                              null: false
    t.string   "status",             limit: 255,                          null: false
    t.integer  "store_id",           limit: 4,                            null: false
    t.decimal  "amount",                         precision: 8,  scale: 2, null: false
    t.string   "remark",             limit: 255
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.integer  "transaction_id",     limit: 4
    t.integer  "diff_id",            limit: 4
    t.decimal  "purchase_amount",                precision: 10
    t.integer  "conversion_id",      limit: 4
    t.boolean  "is_lazada_new_user"
  end

  create_table "user_requests", force: :cascade do |t|
    t.integer  "user_id",     limit: 4,                     null: false
    t.string   "field_type",  limit: 255,                   null: false
    t.text     "field_value", limit: 65535,                 null: false
    t.boolean  "resolved",                  default: false, null: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",            limit: 255,   default: ""
    t.string   "email",           limit: 255,   default: ""
    t.string   "password",        limit: 60,                   null: false
    t.boolean  "is_admin"
    t.string   "remember_token",  limit: 100
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.text     "preferences",     limit: 65535
    t.string   "display_name",    limit: 255
    t.boolean  "newsletter",                    default: true, null: false
    t.integer  "newsletter_type", limit: 4
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  limit: 255,        null: false
    t.integer  "item_id",    limit: 4,          null: false
    t.string   "event",      limit: 255,        null: false
    t.string   "whodunnit",  limit: 255
    t.text     "object",     limit: 4294967295
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "voteable_id",     limit: 4,   null: false
    t.string   "voteable_type",   limit: 255, null: false
    t.integer  "user_id",         limit: 4,   null: false
    t.integer  "vote_value",      limit: 1,   null: false
    t.string   "downvote_reason", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "webviews", force: :cascade do |t|
    t.integer  "viewable_id",   limit: 4,   null: false
    t.string   "viewable_type", limit: 255, null: false
    t.string   "session_id",    limit: 255
    t.integer  "user_id",       limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

end
