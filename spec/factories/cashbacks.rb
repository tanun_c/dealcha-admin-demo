FactoryGirl.define do
  factory :cashback do
    registered {Date.today}
    status 'pending'
    amount 50
    store_id 1
  end
end