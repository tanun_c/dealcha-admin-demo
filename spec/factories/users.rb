FactoryGirl.define do
  factory :user_kinetix, class: User do
    name 'Kinetix'
    email 'jackchai@gmail.com'
    password ''
    is_admin true
  end

  factory :user_turbo, class: User do
    name 'Turbo'
    email 'turby@gmail.com'
    password ''
    is_admin true
  end
end