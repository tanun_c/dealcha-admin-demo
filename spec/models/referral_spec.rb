require "rails_helper"

RSpec.describe Referral, :type => :model do
  
  let(:referree) {create :user_kinetix}
  let(:referrer) {create :user_turbo}
  let(:campaign) {create :current_campaign}

  before :each do
    Referral.destroy_all
    ReferralCampaign.destroy_all
    Cashback.destroy_all
    User.destroy_all
  end

  context "when a cashback was awarded" do
    it "does reward the correct amount" do
      campaign
      referral = create :referral, referrer: referrer, referree: referree
      cashback = build :cashback, :user => referree, :amount => 250, status: :ready
      cashback.save

      expect(referrer.cashback.last[:amount]).to eq(campaign[:award_amount].to_d)
    end
  end

  context "when a new cashback entry is created as ready and more than 200thb" do 
    it "does reward" do
      campaign
      referral = create :referral, referrer: referrer, referree: referree
      cashback = build :cashback, :user => referree, :amount => 250, status: :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(1)
    end

    it "does nothing if already fulfilled" do
      campaign
      referral = create :referral, referrer: referrer, referree: referree
      cashback = create :cashback, :user => referree, :amount => 250, status: :ready

      cashback = build :cashback, :user => referree, :amount => 250, status: :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end
  end

  context "when a pending cashback of more than 200thb change to ready state" do 
    it "does reward" do
      campaign
      referral = create :referral, referrer: referrer, referree: referree
      cashback = create :cashback, :user => referree, :amount => 250, status: :pending
      cashback[:status] = :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(1)
    end

    it "does nothing if already fulfilled" do
      campaign
      referral = create :referral, referrer: referrer, referree: referree
      cashback = create :cashback, :user => referree, :amount => 250, status: :ready
      cashback = create :cashback, :user => referree, :amount => 250, status: :pending
      cashback[:status] = :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end
  end

  context "when a pending cashback change state and make accumulated exceed 200thb" do 
    it "does reward" do
      campaign
      create :referral, referrer: referrer, referree: referree
      create :cashback, :user => referree, :amount => 150, status: :ready
      cashback = create :cashback, :user => referree, :amount => 100, status: :pending
      cashback[:status] = :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(1)
    end

    it "does nothing if already fulfilled" do
      campaign
      create :referral, referrer: referrer, referree: referree
      create :cashback, :user => referree, :amount => 250, status: :ready
      cashback = create :cashback, :user => referree, :amount => 100, status: :pending
      cashback[:status] = :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end
  end

  context "when the total ready cashback does not exceed 200thb" do 
    it "does nothing if one change status" do
      campaign
      create :referral, referrer: referrer, referree: referree
      create :cashback, :user => referree, :amount => 50, status: :ready
      create :cashback, :user => referree, :amount => 50, status: :ready
      
      cashback = create :cashback, :user => referree, :amount => 50, status: :pending
      cashback[:status] = :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end

    it "does nothing if a ready one was created" do
      campaign
      create :referral, referrer: referrer, referree: referree
      create :cashback, :user => referree, :amount => 50, status: :ready
      create :cashback, :user => referree, :amount => 50, status: :ready
      
      cashback = build :cashback, :user => referree, :amount => 50, status: :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end
  end

  context "when the pending cashback was created" do 
    it "does nothing" do
      campaign
      referral = create :referral, referrer: referrer, referree: referree
      cashback = build :cashback, :user => referree, :amount => 250, status: :pending
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end
  end

  context "when the bonus cashback was created" do 
    it "does nothing" do
      campaign
      referral = create :referral, referrer: referrer, referree: referree
      cashback = build :cashback, :user => referree, :amount => 250, status: :bonus
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end
  end

  context "when referral entry does not exist" do
    it "does nothing" do
      campaign
      cashback = build :cashback, :user => referree, :amount => 250, status: :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end
  end

  context "when referral campaign does not exist" do
    it "does nothing" do
      referral = create :referral, referrer: referrer, referree: referree
      cashback = build :cashback, :user => referree, :amount => 250, status: :ready
      expect{cashback.save}.to change{referrer.cashback.count}.by(0)
    end
  end
end