require "rails_helper"

RSpec.describe ReferralCampaign, :type => :model do
  
  before :each do
    ReferralCampaign.destroy_all
  end

  it "can load the current campaign" do
    
    # create the campaign first
    campaign = build(:current_campaign)
    campaign.save
    
    # try finding it
    loaded_campaign = ReferralCampaign.find_active_campaign
    expect(loaded_campaign[:id]).to eq(campaign[:id])
  end

  it "can load the campaign according to the supplied time" do
    campaign = build(:current_campaign)
    campaign.save

    loaded_campaign = ReferralCampaign.find_from_date(Date.today)
    expect(loaded_campaign[:id]).to eq(campaign[:id])
  end

  it "can detect if campaigns are overlapped" do

    campaign = build :current_campaign
    campaign.save
    expect(ReferralCampaign.count).to be(1)

    new_campaign = build :overlapping_campaign
    expect(new_campaign.save).to be_falsy
    expect(ReferralCampaign.count).to be(1)

    newer_campaign = build :not_overlapping_campaign
    newer_campaign.save
    expect(ReferralCampaign.count).to be(2)
    
  end

  it "should limit the number of referree a referrer could possess"
  # might need to think some mechanism for this

  # not the best way but it does help
  it "should track the ip of registering users to prevent bots registering multiple times"

  # can filter out the greans but not smartass who use aws / something
  it "should track the ip of registering users to prevent gaming the system"

  it "should be listed as bonus"

  feature "promo code" do
    it "should allow the admin to create a new promo code"
    it "should be able to verify the code against the promo campaign"
    it "should be able to limit the number of code use"
    it "should be able to set expire date for code"
    it "should be able to set start date for code"
    it "should be renewable"
    it "should have a nice little dashboard that report the code usage"
    it "should have an api for the front end to use"
  end

end