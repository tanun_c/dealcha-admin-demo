$(function() {
  var DATE_FORMAT = 'yy-mm-dd';

  $('.date-filter input[name="start_date"]').datepicker({
    dateFormat: DATE_FORMAT
  });

  $('.date-filter input[name="end_date"]').datepicker({
    dateFormat: DATE_FORMAT
  });

  $('.date-filter input').on('change', function() {
    $('form.date-filter').submit();
  });
});