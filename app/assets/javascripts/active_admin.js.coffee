#= require active_admin/base

#= require jquery
#= require jquery-ui
#= require admin/flot/jquery.flot
#= require admin/flot/jquery.flot.pie
#= require admin/flot/jquery.flot.time

#= require admin/dashboard/date-filter
#= require admin/dashboard/app