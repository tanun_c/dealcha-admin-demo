ActiveAdmin.register PromotionCode do
	menu parent: "Promotion Code"
	belongs_to :promotion_code_campaign
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
	permit_params :id, :promotion_code_campaign_id, :code, :limit
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
