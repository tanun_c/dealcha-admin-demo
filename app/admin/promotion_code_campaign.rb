ActiveAdmin.register PromotionCodeCampaign do
  menu parent: "Promotion Code", priority: 1

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :expiration, :award_amount, 
              promotion_codes_attributes: [ :id, :promotion_code_campaign_id, :code, :limit, :_destroy ]
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  form do |f|
    f.inputs 'Details' do
      f.input :name
      f.input :expiration, as: :datepicker
      # f.input :award_type
      f.input :award_amount
    end

    # t.string :name
    #   t.datetime :expiration
    #   t.string :award_type, default: 'amount' # amount or percent
    #   t.decimal :award_amount, default: 0.0
    #   t.decimal :award_percent, default: 0.0
    #   t.timestamps null: false
    f.inputs do
      f.has_many :promotion_codes, new_record: 'Add a new code', allow_destroy: true do |b|
        b.input :code
        b.input :limit
      end
    end

    f.actions
  end


end
