ActiveAdmin.register_page "Dashboard" do

  # Some common variables
  first_day_of_this_month = Date.today.at_beginning_of_month

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

    @start_date = Date.current.beginning_of_month.beginning_of_day
    @end_date = Date.current.end_of_month.end_of_day

    @start_date = Date.parse(params[:start_date]).beginning_of_day unless params[:start_date].blank?
    @end_date = Date.parse(params[:end_date]).end_of_day unless params[:end_date].blank?

    # columns do
    #   column do
    #     render 'admin/application/summary'
    #   end
    # end

    # columns do
    #   column do
    #     panel "Active Users", title: 'Unique users that generate at least one trip ID between date filter' do
    #       number_with_delimiter(User
    #         .includes(:trip)
    #         .where({ transactions: { occured: @start_date..@end_date  } }).count)
    #     end
    #   end

    #   column do
    #     panel "Converted Users", title: 'Unique users that had at least one cashback registered between date filter'  do
    #       number_with_delimiter(User
    #         .includes(:cashback)
    #         .where.not({
    #           user_cashback: {
    #             user_id: nil
    #           }
    #         })
    #         .where.not({
    #           user_cashback: {
    #             status: %w(bonus payout pending_payout)
    #           }
    #         })
    #         .where({
    #           user_cashback: {
    #             registered: @start_date..@end_date
    #           }
    #         })
    #         .count)
    #     end
    #   end

    #   column do
    #     panel "New Converted Users", title: 'Unique users that have first cashback registered between date filter'  do
    #       number_with_delimiter(User
    #         .includes(:cashback)
    #         .order('user_cashback.registered')
    #         .where.not({ user_cashback: { user_id: nil } })
    #         .where.not({ user_cashback: { status: %w(bonus payout pending_payout) } })
    #         .map do |user|
    #         user.cashback.sort_by(&:registered).first.registered.between?(@start_date, @end_date) ? 1 : 0
    #         end
    #         .inject(0, :+))
    #     end
    #   end

    #   column do
    #     panel 'Lazada New Users' do
    #       begin
    #         new_user = Cashback
    #                      .where(is_lazada_new_user: true)
    #                      .where(registered: @start_date..@end_date)
    #                      .select('transaction_id as id')
    #                      .uniq.to_a.count
    #         existing_user = Cashback
    #                           .where(is_lazada_new_user: false)
    #                           .where(registered: @start_date..@end_date)
    #                           .select('transaction_id as id')
    #                           .uniq.to_a.count

    #         "#{ new_user * 100 / (new_user + existing_user)} % (#{new_user}/#{new_user + existing_user})"
    #       rescue
    #         'N/A'
    #       end
    #     end
    #   end
    # end

    # columns do
    #   column do
    #     panel '% Active Users By Time Period' do
    #       User
    #         .includes(:cashback)
    #         .where.not({ user_cashback: { user_id: nil } })
    #         .where({ user_cashback: { registered: @start_date..@end_date } })
    #         .count * 100 / User.count
    #     end
    #   end

    #   column do
    #     panel '% Total Active Users' do
    #       User
    #         .includes(:cashback)
    #         .where.not({ user_cashback: { user_id: nil } })
    #         .count * 100 / User.count
    #     end
    #   end
    # end

    # columns do
    #   column do
    #     panel "Monthly Summary" do

    #       # Get the last 12 months
    #       months = (0..11).map{|i| (Date.today - i.month).beginning_of_month}

    #       monthly_value = controller.monthly_stat(months)
    #       monthly_value.each do |value|
    #         if value[:trips_num].to_i.zero? &&
    #            value[:cashback_sum].to_i.zero? &&
    #            value[:approved_revenue_sum].to_i.zero? &&
    #            value[:pending_revenue_sum].to_i.zero? &&
    #            value[:bonus_sum].to_i.zero? &&
    #            value[:payout_sum].to_i.zero? &&
    #            value[:order_sum].to_i.zero? &&
    #            value[:order_uniq_sum].to_i.zero? &&
    #            value[:user_number].to_i.zero?

    #            monthly_value.delete(value)
    #         end
    #       end

    #       table_for monthly_value do
    #         column(:month) { |month| "#{month[:month].strftime('%b %Y')}" }
    #         column(:trips, :trips_num)
    #         column(:cashback, :cashback_sum)
    #         column(:approved_revenue, :approved_revenue_sum)
    #         column(:pending_revenue, :pending_revenue_sum)
    #         column(:bonus, :bonus_sum)
    #         column(:payout, :payout_sum)
    #         column(:order, :order_sum)
    #         column(:uniq_order, :order_uniq_sum)
    #         column(:new_users, :user_number)
    #       end # table

    #     end # panel
    #   end
    # end

    # # New update and reminder part
    # columns do
    #   column do
    #     panel "Recent Shopping Trips" do
    #       table_for Trip.last(25).reverse do
    #         column :user
    #         column :store
    #         column :date, :occured
    #       end # table
    #     end # panel
    #   end # column

    #   column do
    #     panel "Pending Payout Requests" do
    #       table_for Cashback.where('status = "payout_pending"').reverse do |cashback|
    #         column(:user)
    #         column(:amount)
    #         column(:registered)
    #         column 'Action' do |cashback|
    #           link_to 'Edit', edit_admin_cashback_path(cashback)
    #         end
    #       end # table
    #     end # panel

    #     panel 'User Ranking' do
    #       @users_cashback = User
    #                           .joins(:cashback)
    #                           .where.not({ user_cashback: { status: %w(bonus payout pending_payout) } })
    #                           .select('users.email as email, sum(user_cashback.amount) as cashback_amount')
    #                           .group('email').map { |m| { email: m.email, cashback_amount: m.cashback_amount.to_f } }
    #       @users_payout = User
    #                         .joins(:cashback)
    #                         .where({ user_cashback: { status: %w(payout) } })
    #                         .select('users.email as email, sum(user_cashback.amount) as payout_amount')
    #                         .group('email').map { |m| { email: m.email, payout_amount: m.payout_amount.to_f } }

    #       @users = (@users_cashback + @users_payout)
    #                  .group_by { |user| user[:email] }
    #                  .map { |k, v| v.reduce(:merge) }
    #                  .sort_by { |user| [-(user[:cashback_amount] || 0), -(user[:payout_amount] || 0)] }

    #       if params[:order] && @users.first[params[:order].gsub('_desc', '').to_sym] != nil
    #         @users = @users.sort_by { |user| -(user[params[:order].gsub('_desc', '').to_sym] || 0) }
    #       end

    #       table_for @users.first(10), sortable: :true do
    #         column(:email)
    #         column(:cashback_amount) { |user| number_with_delimiter(user[:cashback_amount]) }
    #         column(:payout_amount) { |user| number_with_delimiter(user[:payout_amount]) }
    #       end
    #     end

    #     panel 'Store Ranking' do
    #       @stores = Store
    #                   .all
    #                   .map { |s| { name: s.name, shopping_trips: s.trips.count, cashbacks: s.cashbacks.where.not(status: %w(bonus)).sum(:amount) } }
    #                   .sort_by { |store| [-store[:shopping_trips], -store[:cashbacks]] }

    #       if params[:order] && @stores.first[params[:order].gsub('_desc', '').to_sym]
    #         @stores = @stores.sort_by { |s| -(s[params[:order].gsub('_desc', '').to_sym] || 0) }
    #       end

    #       table_for @stores.first(10), sortable: :true do
    #         column(:name)
    #         column(:shopping_trips) { |store| number_with_delimiter(store[:shopping_trips]) }
    #         column(:cashbacks) { |store| number_with_delimiter(store[:cashbacks]) }
    #       end
    #     end
    #   end # column

    # end # columns

  end # content

  # for private functions
  controller do
    include ActionView::Helpers::NumberHelper

    def index
      @start_date = Date.current.beginning_of_month.beginning_of_day
      @end_date = Date.current.end_of_month.end_of_day

      @start_date = Date.parse(params[:start_date]).beginning_of_day unless params[:start_date].blank?
      @end_date = Date.parse(params[:end_date]).end_of_day unless params[:end_date].blank?

      @approved_cashback = Cashback
                             .where(status: ["approved", "Approved"], registered: @start_date..@end_date)
      @pending_cashback = Cashback
                            .where(status: ["pending", "Pending"], registered: @start_date..@end_date)
      @rejected_cashback = Cashback
                             .where(status: ["rejected"], registered: @start_date..@end_date)
    end

    # add more monthly stat here
    def monthly_stat(months)
      months.map do |month|
        end_of_month = month.end_of_month.end_of_day

        trips_num     = number_with_delimiter(Trip
                          .where(occured: month..end_of_month)
                          .count)

        cashback_sum  = number_with_delimiter(Cashback
                          .where('status NOT LIKE "%payout%"')
                          .where.not(status: 'bonus')
                          .where(registered: month..end_of_month)
                          .sum(:amount))

        approved_revenue_sum = number_with_delimiter(Conversion
                    .includes(:trip)
                    .where({ transactions: { occured: month..end_of_month } })
                    .where(status: 'approved')
                    .sum(:amount))

        pending_revenue_sum = number_with_delimiter(Conversion
                                 .includes(:trip)
                                 .where({ transactions: { occured: month..end_of_month } })
                                 .where(status: %w(Pending pending))
                                 .sum(:amount))

        bonus_sum = number_with_delimiter(Cashback
                      .where(status: 'bonus')
                      .where(registered: month..end_of_month)
                      .sum(:amount))

        payout_sum = number_with_delimiter(Cashback
                      .where(status: 'payout')
                       .where(registered: month..end_of_month)
                       .sum(:amount))

        order_sum = number_with_delimiter(Cashback
                      .where.not(transaction_id: nil)
                      .where(registered: month..end_of_month)
                      .count)

        order_uniq_sum = number_with_delimiter(Cashback
                           .where.not(transaction_id: nil)
                           .where(registered: month..end_of_month)
                           .select('transaction_id as id').uniq.to_a.count)

        user_number   = number_with_delimiter(User
                          .where(created_at: month..end_of_month)
                          .count)

        # Include hash into the array to return
        {
          month: month,
          trips_num: trips_num,
          cashback_sum: cashback_sum,
          approved_revenue_sum: approved_revenue_sum,
          pending_revenue_sum: pending_revenue_sum,
          bonus_sum: bonus_sum,
          payout_sum: payout_sum,
          order_sum: order_sum,
          order_uniq_sum: order_uniq_sum,
          user_number: user_number
        }
      end
    end
  end

end
