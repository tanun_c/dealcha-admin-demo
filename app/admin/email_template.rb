ActiveAdmin.register EmailTemplate do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :key, :description, :locale, :body
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  form do |f|
    
    inputs 'Details' do
      f.input :key, label: "Key"
      f.input :description, label: "Key"
      f.input :locale, :as => :select, :collection => ["th", "en"]
      f.input :body, :as => :ckeditor
    end
  
    f.actions
  end

end
