# ActiveAdmin.register Conversion do

# # See permitted parameters documentation:
# # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
# #
# # permit_params :list, :of, :attributes, :on, :model
# #
# # or
# #
# # permit_params do
# #   permitted = [:permitted, :attributes]
# #   permitted << :other if resource.something?
# #   permitted
# # end

#   filter :store, as: :select, collection: Store.all.map(&:name), incluce_blank: false
#   filter :search_store_name, as: :string
#   filter :trip
#   filter :cashback
#   filter :purchase
#   filter :amount
#   filter :status
#   filter :info
#   filter :created_at
#   filter :updated_at

#   csv do
#     column :status
#     column "Trip" do |conversion|
#       conversion.trip.id
#     end
    
#     column "Occur" do |conversion|
#       conversion.trip.occured
#     end

#     column "Store" do |conversion|
#       conversion.trip.store.name
#     end

#     column :purchase
#     column :amount

#     column "Cashback" do |conversion|
#       conversion.cashback.amount if conversion.cashback
#     end
#   end

#   index do
#     # selectable_column
#     # column :id
#     column :status
#     column :trip
#     column "Occur", :sortable => true do |conversion|
#       conversion.trip.occured
#     end

#     column "Store", :sortable => true do |conversion|
#       conversion.trip.store.name
#     end

#     column :purchase
#     column :amount

#     column "Cashback", :sortable => false do |conversion|
#       conversion.cashback.amount
#     end
#   end

#   controller do
#     def index
#       super do
#         if params[:q]
#           if params[:q][:store]
#             @conversions = @conversions.includes(trip: [:store]).where({ stores: { name: params[:q][:store] } })
#           end

#           if params[:q][:search_store_name]
#             @conversions = @conversions.joins(trip: [:store]).where('stores.name LIKE ?', "%#{params[:q][:search_store_name]}%")
#           end
#         end
#       end
#     end
#   end

# end
