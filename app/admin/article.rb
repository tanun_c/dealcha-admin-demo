ActiveAdmin.register Article do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  permit_params do
    permitted = [:key, :thumbnail_image, :content_type, :title, :content]
  end

  csv do
    column :id
    column :title
    column(:creator) { |article| article.creator.email }
    column :content_type
    column :key
  end

  index do
    selectable_column
    column :id
    column :title
    column :creator
    column :content_type
    column :key
    actions
  end

  form do |f|
    
    inputs 'Details' do
      f.input :key, label: "Key (only for page)"
      f.input :thumbnail_image, :as => :file, :hint => f.template.image_tag(f.object.thumbnail_image.url(:original))
      f.input :content_type, :as => :select, :collection => ["page", "article"]
      f.input :title
      f.input :content, :as => :ckeditor
      f.input :creator, :as => :select
    end
  
    f.actions
  end

end
