ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  DATE_FORMAT = { day: '%Y-%m-%d', month: '%Y-%m-01', year: '%Y-01-01' }

  filter :id
  # filter :cashback
  # filter :trip
  # filter :referred_entry
  # filter :referring_entry
  filter :name
  filter :email
  filter :is_admin
  filter :created_at
  filter :updated_at
  filter :preferences
  filter :display_name
  filter :newsletter
  filter :newsletter_type
  # filter :graph_type,
  #        label: 'Graph type',
  #        as: :select,
  #        collection: -> { %w(pie bars lines) },
  #        include_blank: false

  # filter :date_part,
  #        label: 'Date part',
  #        as: :select,
  #        collection: -> { %w(day month year) },
  #        include_blank: false

  permit_params :email, :name

  index do
    selectable_column
    column :id
    column :email
    column :name
    column :status
    actions

    # graph_type = 'lines'
    # date_part = 'day'

    # if params[:q]
    #   graph_type = params[:q][:graph_type]
    #   date_part = params[:q][:date_part]
    # end


    # date_format = DATE_FORMAT["#{date_part}".to_sym]
    # json_data = []

    # chart_data = users
    #                 .group("DATE_FORMAT(users.created_at, '#{date_format}')")
    #                 .count

    # if graph_type == 'pie'
    #   chart_data.each do |key, value|
    #     json_data << {
    #       label: key.to_date.strftime(Date::DATE_FORMATS[:rfc822]),
    #       data: value
    #     }
    #   end
    # else
    #   data = []
    #   chart_data.each do |key, value|
    #     data << [ key, value ]
    #   end
    #   json_data = [{ label: graph_type, data: data, color: "##{SecureRandom.hex(3)}" }]
    # end

    # render partial: 'admin/application/charts',
    #        locals: {
    #          data: json_data.to_json,
    #          graph_type: graph_type,
    #          date_part: date_part
    #        }
  end

  show do
    attributes_table do
      row :id
      row :name
      row :email
      row :is_admin
      row :created_at
    end

    columns do
      column do
        panel "Cashbacks" do
          table_for user.cashback.reverse do
            column(:id) do |cashback|
              span "<a href=\"/admin/cashbacks/#{cashback.id}\">#{cashback.id}</a>".html_safe
            end
            column :registered
            column :store
            column :amount
            column :status
          end
        end
      end

      column do
        panel "Trips" do
          paginated_collection(user.trip.order(occured: :desc).page(params[:page]).per(15), download_links: false) do
            table_for collection do
              column :occured
              column :store
            end
          end
        end
      end
    end
    active_admin_comments
  end
end
