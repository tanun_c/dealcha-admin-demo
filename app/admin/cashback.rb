ActiveAdmin.register Cashback do

  permit_params :user_id, :registered, :status, :amount, :remark, :store_id, :transaction_id, :purchase_amount

  filter :id
  filter :user_email, as: :string
  filter :store
  filter :registered
  filter :status
  filter :amount
  filter :remark
  filter :created_at
  filter :updated_at
  filter :purchase_amount

  # filter :graph_type,
  #        label: 'Graph type',
  #        as: :select,
  #        collection: -> { %w(pie bars lines) },
  #        include_blank: false

  # filter :date_part,
  #        label: 'Date part',
  #        as: :select,
  #        collection: -> { %w(day month year) },
  #        include_blank: false

  csv do
    column :id
    column :registered
    column(:user) { |cashback| cashback.user.email }
    column(:store) { |cashback| cashback.store.name }
    column :status
    column :amount
    column :purchase_amount
  end

  after_create do |cashback|
    # CashbackMailer.new_cashback_email(cashback).deliver_now
  end

  before_update do
    PaperTrail.whodunnit = current_admin_user.email
  end

  collection_action :import_csv

  collection_action :connect_lazada, method: :get do
    CashbackModule.set_user current_admin_user
    
    (1..20).each do |page|
      CashbackModule.connect_lazada(page)
    end

    redirect_to collection_path #, notice: "API connected successfully! #{created} new entries and #{total - created} updated"
  end

  # Upload conversion CSV and convert them into cashback entries in DB
  collection_action :import_csv_upload, method: :post do

    # for identifying the batch -- in case the conversion does not have sequence no
    # timestamp = Time.now.to_i
    
    # read file
    uploaded_io = params[:file]
    file = File.open(uploaded_io.tempfile, "r")
    data = file.read
    file.close

    # split row
    data = data.split "\n"

    row_number = 0
    master_hash = []
    data.each_with_index do |data_row, index|
      
      # remove end of line artifact
      data_row.gsub! "\r", ''

      # read header row
      if index == 0
        data_row.gsub! " ", '' # some user error will cause the upload to break -- this will prevent it for some degree
        master_hash = data_row.split ","
        next
      end

      # Data row start here
      result = {}

      items = data_row.split ","

      items.each_with_index do |item, index|
        key = master_hash[index].to_sym
        result[key] = item
      end

      cashback = create_cashback result
      next if not cashback

      # for revenue tracking
      conversion = Conversion.where(
        diff_id: result[:diff_id],
        trip_id: result[:transaction_id]
      ).first

      if not conversion
        conversion = Conversion.new(diff_id: result[:diff_id], trip_id: result[:transaction_id]) 
      end

      conversion.purchase = result[:purchase_amount].to_d
      conversion.amount = result[:payout].to_d
      conversion.status = result[:status]
      conversion.info = items.to_json
      conversion.save

      cashback.conversion_id = conversion.id
      cashback.save

    end
    
    redirect_to collection_path, notice: "CSV imported successfully!"
  end
  

  # Topbar Button
  action_item :import do
    link_to 'Import Accesstrade CSV', import_csv_admin_cashbacks_path
  end

  action_item :load_lazada do
    link_to 'Load Lazada', connect_lazada_admin_cashbacks_path
  end

  # Mostly functions from import_csv_upload action
  controller do
    def create_cashback (hash)
      
      # No transaction no load
      if not hash[:transaction_id]
        return false 
      end

      # find transaction and store
      transaction = find_transaction hash
      if transaction.nil? # transaction not found -- might occur when using with dev environment
        return false 
      end

      # populate the data
      store = transaction.store
      hash[:store_id] = store.id

      return false if not transaction[:user_id]
      hash[:user_id] = transaction[:user_id]

      hash[:purchase_amount] = hash[:purchase_amount].to_d
      hash[:registered] = determine_date hash
      hash[:amount] = calculate_cashback hash, store, transaction

      cashback = find_existing_or_new hash
      
      hash[:status] = "approved" if hash[:status].downcase == "ready"
      cashback[:status] = hash[:status].downcase

      cashback.set_paper_trail_admin_user(current_admin_user.email)
      cashback.save
      return cashback
    end


    def calculate_cashback (hash, store, transaction)
      if transaction[:cashback_amount]
        hash[:purchase_amount] * transaction[:cashback_amount] / 100
      else
        hash[:purchase_amount] * store[:cashback] / 100
      end

    end

    def find_existing_or_new (hash)
      cashback = Cashback.where(
        # registered: hash[:registered], 
        # purchase_amount: hash[:purchase_amount], 
        transaction_id: hash[:transaction_id],
        diff_id: hash[:diff_id]
      ).first

      if not cashback
        cashback = Cashback.new hash.except :payout
      end

      return cashback
    end

    # Set Timeliness format setting in app.rb in case Lazada did some funny thing with their date ... again (ugh...)
    def determine_date (hash)
      Timeliness.parse(hash[:registered])
    end

    def find_transaction (hash)
      return Trip.find_by_id(hash[:transaction_id])
    end
  end
  
  form do |f|
    
    inputs 'Details' do
      f.input :user, :label_method => :company_name, label: 'User'
      f.input :user, label: 'User Email', as: :string
      f.input :registered, :as => :datepicker
      f.input :status, :as => :select, :collection => ["ready", "pending", "rejected", "payout", "bonus"]
      f.input :amount
      f.input :remark
      f.input :store, :as => :select, :label_method => :dropdown_name, label: "Store"
      f.input :transaction_id
      f.input :purchase_amount
    end
  
    f.actions

    render partial: 'admin/application/cashback_form'
  end

  batch_action :set_approved_status_to do |ids|
    Cashback.find(ids).each do |cashback|
      cashback.status = "approved"
      cashback.save
    end
    redirect_to collection_path, alert: "Successfully change #{ids.length} cashback entries to 'approved'."
  end

  index do
    selectable_column
    column :id
    column :registered
    column :user #, as: :email
    column :status
    column :amount
    column :store
    column :transaction_id
    column :purchase_amount
    # column(:admin_update) { |cashback| cashback.versions.last ? cashback.versions.last.whodunnit : '' }
    actions

    # graph_type = 'lines'
    # date_part = 'day'

    # if params[:q]
    #   graph_type = params[:q][:graph_type]
    #   date_part = params[:q][:date_part]
    # end


    # date_format = DATE_FORMAT["#{date_part}".to_sym]
    # json_data = []

    # chart_data = cashbacks
    #                 .group("DATE_FORMAT(user_cashback.registered, '#{date_format}')")
    #                 .count

    # if graph_type == 'pie'
    #   chart_data.each do |key, value|
    #     begin
    #       key = key.to_date.strftime(Date::DATE_FORMATS[:rfc822])
    #     rescue
    #       key = key
    #     end
    #     json_data << {
    #       label: key,
    #       data: value
    #     }
    #   end
    # else
    #   data = []
    #   chart_data.each do |key, value|
    #     data << [ key, value ]
    #   end
    #   json_data = [{ label: graph_type, data: data, color: "##{SecureRandom.hex(3)}" }]
    # end

    # render partial: 'admin/application/charts',
    #        locals: {
    #          data: json_data.to_json,
    #          graph_type: graph_type,
    #          date_part: date_part
    #        }
  end

end