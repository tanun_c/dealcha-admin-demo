ActiveAdmin.register Trip do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  csv do
    column :id
    column(:user) { |trip| trip.user ? trip.user.email : nil }
    column(:store) { |trip| trip.store ? trip.store.name : nil }
    column :created_at
  end

  filter :id
  filter :store
  filter :user
  filter :occured
  filter :destination
  filter :cashback
  filter :created_at
  filter :updated_at
  # filter :graph_type,
  #        label: 'Graph type',
  #        as: :select,
  #        collection: -> { %w(pie bars lines) },
  #        include_blank: false

  # filter :date_part,
  #        label: 'Date part',
  #        as: :select,
  #        collection: -> { %w(day month year) },
  #        include_blank: false

  index do
    selectable_column
    id_column
    column :user
    column :store
    # column :current_sign_in_at
    # column :sign_in_count
    column :created_at
    actions

    # graph_type = 'lines'
    # date_part = 'day'

    # if params[:q]
    #   graph_type = params[:q][:graph_type]
    #   date_part = params[:q][:date_part]
    # end


    # date_format = DATE_FORMAT["#{date_part}".to_sym]
    # json_data = []

    # chart_data = trips
    #                .group("DATE_FORMAT(transactions.occured, '#{date_format}')")
    #                .count

    # if graph_type == 'pie'
    #   chart_data.each do |key, value|
    #     json_data << {
    #       label: key.to_date.strftime(Date::DATE_FORMATS[:rfc822]),
    #       data: value
    #     }
    #   end
    # else
    #   data = []
    #   chart_data.each do |key, value|
    #     data << [ key, value ]
    #   end
    #   json_data = [{ label: graph_type, data: data, color: "##{SecureRandom.hex(3)}" }]
    # end

    # render partial: 'admin/application/charts',
    #        locals: {
    #          data: json_data.to_json,
    #          graph_type: graph_type,
    #          date_part: date_part
    #        }
  end

end