ActiveAdmin.register ReferralCampaign do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :campaign_start, :campaign_end, :award_amount
  #
  # or
  #
  form do |f|
      
    inputs 'Details' do
      f.input :name
      f.input :campaign_start, :as => :datepicker
      f.input :campaign_end, :as => :datepicker
      f.input :award_amount
    end

    f.actions
  end


end
