ActiveAdmin.register Store do

  permit_params :name, :description, :is_featured, 
                :flag, :cashback, :url, :image, :thumbnail_image, :provider, :main_url, :deal_url

  filter :name
  filter :description
  filter :flag
  filter :is_featured
  filter :cashback
  filter :created_at
  filter :updated_at
  filter :url
  filter :image
  # filter :graph_type,
  #        label: 'Graph type',
  #        as: :select,
  #        collection: -> { %w(pie bars lines) },
  #        include_blank: false

  # filter :date_part,
  #        label: 'Date part',
  #        as: :select,
  #        collection: -> { %w(day month year) },
  #        include_blank: false

  form do |f|
    inputs 'Details' do
      f.input :name
      f.input :description
      f.input :is_featured
      f.input :flag
      f.input :cashback
      f.input :url
      f.input :provider, :as => :select, :collection => ['accesstrade', 'lazada', 'cj']
      f.input :main_url
      f.input :deal_url
      f.input :thumbnail_image, :as => :file, :hint => f.template.image_tag(f.object.thumbnail_image.url(:original))
    end

    f.actions
  end

  index do
    selectable_column
    column :id
    column :name
    column :is_featured
    column :flag
    column :cashback
    actions


    # graph_type = 'lines'
    # date_part = 'day'

    # if params[:q]
    #   graph_type = params[:q][:graph_type]
    #   date_part = params[:q][:date_part]
    # end


    # date_format = DATE_FORMAT["#{date_part}".to_sym]
    # json_data = []

    # chart_data = stores
    #                 .group("DATE_FORMAT(stores.created_at, '#{date_format}')")
    #                 .count

    # if graph_type == 'pie'
    #   chart_data.each do |key, value|
    #     begin
    #       json_data << {
    #         label: key.to_date.strftime(Date::DATE_FORMATS[:rfc822]),
    #         data: value
    #       }
    #     rescue
    #       json_data << { label: key, data: value }
    #     end
    #   end
    # else
    #   data = []
    #   chart_data.each do |key, value|
    #     data << [ key, value ]
    #   end
    #   json_data = [{ label: graph_type, data: data, color: "##{SecureRandom.hex(3)}" }]
    # end

    # render partial: 'admin/application/charts',
    #        locals: {
    #          data: json_data.to_json,
    #          graph_type: graph_type,
    #          date_part: date_part
    #        }
  end
end
