# ActiveAdmin.register_page "Cohort" do

#   content title: proc{ I18n.t("active_admin.dashboard") } do
#     columns do
#       column do
#         render 'admin/application/active_user_cohort_table'
#       end
#     end

#     columns do
#       column do
#         render 'admin/application/converted_user_cohort_table'
#       end
#     end

#     columns do
#       column do
#         render 'admin/application/repeat_user_cohort_table'
#       end
#     end
#   end

#   controller do

#     def index
#       @months = (0..11).map { |i| (Date.today - i.month).beginning_of_month }.sort

#       @cohort_active_users = @months.map do |month|
#         activations = User.where(created_at: month.beginning_of_month..month.end_of_month.end_of_day).select(:id)

#         activities = @months.map do |m|
#           {
#             month: m,
#             count: User
#                      .includes(:trip)
#                      .select('users.id as id, count(transactions.id) as count')
#                      .where({ transactions: { occured: m.beginning_of_month..m.end_of_month.end_of_day } })
#                      .where('users.id in (?)', activations)
#                      .group('id')
#                      .to_a.count
#           }
#         end
#         {
#           activations_month: month,
#           activations_count: activations.count,
#           activities_count: activities
#         }
#       end

#       @cohort_converted_users = @months.map do |month|
#         activations = User.where(created_at: month.beginning_of_month..month.end_of_month.end_of_day).select(:id)

#         activities = @months.map do |m|
#           {
#             month: m,
#             count: User
#                      .includes(:cashback)
#                      .select('users.id as id, count(user_cashback.id) as count')
#                      .where.not({ user_cashback: { status: %w(bonus payout pending_payout) } })
#                      .where.not({ user_cashback: { user_id: nil } })
#                      .where({ user_cashback: { registered: m.beginning_of_month..m.end_of_month.end_of_day } })
#                      .where('users.id in (?)', activations)
#                      .group('id')
#                      .to_a.count
#           }
#         end
#         {
#           activations_month: month,
#           activations_count: activations.count,
#           activities_count: activities
#         }
#       end

#       @cohort_repeat_users = @months.map do |month|
#         activations = User
#                         .includes(:cashback)
#                         .where.not({ user_cashback: { user_id: nil } })
#                         .where.not({ user_cashback: { status: %w(bonus payout pending_payout) } })
#                         .map do |user|
#           user.cashback.sort_by(&:registered).first.registered.between?(month.beginning_of_month, month.end_of_month.end_of_day) ? user.id : nil
#         end.compact

#         activities = @months.map do |m|
#           {
#             month: m,
#             count: User
#                      .joins(:cashback)
#                      .select('users.id as id, count(user_cashback.id) as count')
#                      .where.not({ user_cashback: { status: %w(bonus payout pending_payout) } })
#                      .where.not({ user_cashback: { user_id: nil } })
#                      .where({ user_cashback: { registered: m.beginning_of_month..m.end_of_month.end_of_day } })
#                      .where('users.id in (?)', activations)
#                      .group('id')
#                      .to_a.count
#           }
#         end
#         {
#           activations_month: month,
#           activations_count: activations.count,
#           activities_count: activities
#         }
#       end
#     end
#   end
# end
