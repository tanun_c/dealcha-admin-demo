class CashbackMailer < ApplicationMailer
	default from: 'bo@dealcha.com'
 
	def new_cashback_email(cashback)

		# Grab the template first and parse it
		template = EmailTemplate::where(:key => :new_cashback_email).first
		@template = Liquid::Template.parse(template.body)
		@user = cashback.user
		@store = cashback.store

		# Rendering
		@content = @template.render('user' => @user, 'cashback_amount' => cashback[:amount], 'store_name' => @store[:name])
		# @url  = 'http://example.com/login'
		mail(to: 'tanun.c@gmail.com', subject: 'คุณได้รับเงินคืน -- You\'ve received a new Cashback!')
	end
end