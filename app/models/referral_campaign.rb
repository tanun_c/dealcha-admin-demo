class ReferralCampaign < ActiveRecord::Base

  validates :campaign_start, :campaign_end, overlap: true
  
  def self.find_active_campaign
    self.where('campaign_start < :today AND campaign_end > :today', today: Date.today).first
  end 

  def self.find_from_date (query_date)
    self.where('campaign_start < :day AND campaign_end > :day', day: query_date).first
  end

end