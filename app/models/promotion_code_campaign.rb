class PromotionCodeCampaign < ActiveRecord::Base
	has_many :promotion_codes
	accepts_nested_attributes_for :promotion_codes, allow_destroy: true
end