class Referral < ActiveRecord::Base
  belongs_to :referrer, class_name: 'User', foreign_key: "referrer_id"
  belongs_to :referree, class_name: 'User', foreign_key: "referree_id"
end