class User < ActiveRecord::Base
  # Required to use custom filter in active admin
  # ransacker :graph_type
  # ransacker :date_part

	has_many :cashback, foreign_key: "user_id"
	has_many :trip, foreign_key: "user_id"
	has_one :referred_entry, class_name: 'Referral', foreign_key: "referree_id"
	has_one :referring_entry, class_name: 'Referral', foreign_key: "referrer_id"
	
	def display_name
		if self.email?
			"#{self.id}: #{self.email}"
		else
			"#{self.id}: #{self.name}"
		end
	end

end
