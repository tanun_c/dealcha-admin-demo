class Cashback < ActiveRecord::Base
  self.table_name = "user_cashback"

  # has_paper_trail

  # Required to use custom filter in active admin
  # ransacker :graph_type
  # ransacker :date_part

  after_create :check_referral
  after_update :check_referral
  
  belongs_to :user
  belongs_to :store
  belongs_to :conversion

  def set_status(new_status)
    self[:status] = new_status
  end

  def set_paper_trail_admin_user(admin_user)
    PaperTrail.whodunnit = admin_user
  end

  private def check_referral

    return if self[:store_id] == 0 or self[:status] == 'bonus' or self[:status] == 'pending'

    user = self.user
    return if not user    

    # Check the referral entry, if nothing (not referred) then return
    return if not user.referred_entry
    return if user.referred_entry[:claimed]

    # Get the referrer
    referrer = user.referred_entry.referrer

    # Check if he exists
    return if not referrer

    # Check if total ready cashback exceed or not
    total_ready_cashback = user.cashback.where(:status => :ready).sum(:amount)
    return if total_ready_cashback < 200

    # Load campaign, no campaign = no reward
    campaign = ReferralCampaign.find_from_date(user[:created_at])
    return if not campaign

    # Create cashback entry to the referrer
    cashback = Cashback.new
    cashback[:user_id] = referrer[:id]
    cashback[:registered] = Date.today
    cashback[:status] = 'bonus'
    cashback[:store_id] = 0
    cashback[:amount] = campaign[:award_amount]
    cashback[:remark] = 'Referral bonus'
    cashback.save

    user.referred_entry[:claimed] = true
    user.referred_entry.save

  end
end