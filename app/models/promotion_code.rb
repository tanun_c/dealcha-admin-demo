class PromotionCode < ActiveRecord::Base
	belongs_to :promotion_code_campaign
	has_many :promotion_code_usages
end
