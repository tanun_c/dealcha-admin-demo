class Store < ActiveRecord::Base
	# Required to use custom filter in active admin
	# ransacker :graph_type
	# ransacker :date_part

	has_attached_file :thumbnail_image
	validates_attachment :thumbnail_image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] } 
	before_save :update_thumbnail

	has_many :trips
	has_many :cashbacks

	private def update_thumbnail
		self[:image] = self.thumbnail_image.url(:original) if self.thumbnail_image
	end

	def display_name
		if self.name.length <= 40
			self.name
		else
			"#{self.name[0..40]}..."
		end
	end
end
