class Trip < ActiveRecord::Base
	self.table_name = :transactions

	# Required to use custom filter in active admin
	# ransacker :graph_type
	# ransacker :date_part

	def store_id
		@store_id
	end

	belongs_to :store
	belongs_to :user

	def user_id
		@user_id
	end
end