class Conversion < ActiveRecord::Base
  belongs_to :trip
  has_one :cashback

  # Required to use custom filter in active admin
  # ransacker :store
  # ransacker :search_store_name
end
