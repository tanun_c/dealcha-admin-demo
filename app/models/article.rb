class Article < ActiveRecord::Base
  has_attached_file :thumbnail_image
  validates_attachment :thumbnail_image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png"] } 
  belongs_to :creator, class_name: 'User'

  before_save :update_thumbnail
  
  private def update_thumbnail
  	self[:thumbnail] = self.thumbnail_image.url(:original)
  end

end
