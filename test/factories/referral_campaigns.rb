FactoryGirl.define do
  factory :current_campaign, class: ReferralCampaign do
    name 'current_campaign'
    campaign_start {Date.today.prev_month}
    campaign_end {Date.today.next_month}
    award_amount 30.0
    grace_period 3
    condition :cashback
  end

  factory :overlapping_campaign, class: ReferralCampaign do
    name 'overlapping_campaign'
    campaign_start {Date.today.prev_month.prev_month}
    campaign_end {Date.today}
    award_amount 40.0
    grace_period 3
    condition :cashback
  end

  factory :not_overlapping_campaign, class: ReferralCampaign do
    name 'overlapping_campaign'
    campaign_start {Date.today.prev_month.prev_month.prev_month}
    campaign_end {Date.today.prev_month.prev_month}
    award_amount 60.0
    grace_period 3
    condition :cashback
  end
end
